function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end

function matrix_pot(M, p)
    dim = size(M)
    c = zeros(dim[1], dim[2])
    for i in 1:dim[1]
        c[i, i] = 1
    end
    for i in 1:p
        c = multiplica(c, M)
    end
    return c
end
    
function matrix_pot_by_squaring(M, p)
    dim = size(M)
    if p == 1
        return M
    elseif p % 2 == 0
        aux = matrix_pot_by_squaring(M, p/2)
        aux = multiplica(aux, aux)
        return aux
    else
        return multiplica(M, matrix_pot_by_squaring(M, p-1))
    end
end
